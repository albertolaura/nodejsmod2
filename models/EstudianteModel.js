
var mongoose = require('mongoose');
var { Schema } = mongoose;

var EstudianteSchema = new Schema({
    nombres: String, 
    apellidoPaterno: String, 
    numeroDocumento: String, 
    curso: String,
});

const Estudiante = mongoose.model('estudiante', EstudianteSchema );

module.exports = Estudiante; 