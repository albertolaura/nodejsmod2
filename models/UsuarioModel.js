
var mongoose = require('mongoose');
var { Schema } = mongoose;

var UsuarioSchema = new Schema({
    usuario: String, 
    contrasena: String,
    permisos: Array
});

const Usuario = mongoose.model('usuario', UsuarioSchema );

module.exports = Usuario; 