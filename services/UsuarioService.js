
const {UsuarioRepository} = require('../repositories');

const login = async ( usuario, contrasena ) => {

    const existeUsuario = await UsuarioRepository.findOne({ usuario, contrasena })
    return existeUsuario;
}


const findAll = async ( query ) => {

    // Modificar query y adicionando un parametro mas ..

    const usuario = await UsuarioRepository.findAndCountAll(query);
    return usuario;
}

const guardar = async (datos) => {
    const usuario= await UsuarioRepository.createOrUpdate(datos);
    return usuario;
}

const eliminar = async (_id) => {
    const usuarioEliminado = await UsuarioRepository.deleteItem(_id);
    return usuarioEliminado;
}



module.exports = {
    findAll,
    guardar, 
    eliminar, 
    login
}
