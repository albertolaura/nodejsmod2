const express = require('express');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 3000;
const bodyParser =  require('body-parser');

const { EstudianteRoute, AuthRoute, UsuarioRoute } = require('./routes');

// conexion
const mongoose = require('mongoose');       //db:mongoose
const {db} = require('./config');
                                    // Para pruebas, luego deshabilitar
// mongoose.set('debug', true);     // Habilita DEBUG en  monto

//mongoose.connect('mongodb://localhost:27017/clase-final', { useNewUrlParser: true, useUnifiedTopology: true } );
mongoose.connect( db.urlConexion , { useNewUrlParser: true, useUnifiedTopology: true } );
    
app.use(cors());
app.use(bodyParser.json());

app.use(AuthRoute);
app.use(EstudianteRoute);
app.use(UsuarioRoute);

app.listen(port, () => {
  console.log(`===> Funcionando en el puerto ${port}`);
});
