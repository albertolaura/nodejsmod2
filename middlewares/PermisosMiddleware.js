
const {app} = require('../config');


module.exports = {

  verificarPermisos: (permiso) => async (req, res, next) => {
    try {
        
        const { permisos, usuario } = req.usuario;

        console.log('Permisos, usuario = ', req.usuario);
        
        if (!permisos.includes (permiso) ) {
            throw new Error( usuario + ' - No tiene permisos necesarios : ');
        }
        
        next();
        
    } catch (error) {
        res.status(401).json({
        finalizado: false,
        mensaje: error.message,
        datos: null
        })
    }



  }
} 

