const jwt = require("jsonwebtoken");
const {app} = require('../config');

// sacamos del HEADER la variable autorization 
const verificarToken = async (req, res, next) => {
  try {
    const { authorization } = req.headers;
    if (!authorization) {
      throw new Error('Cabecera No autorizado.');
    }

             // Se decodifica el token
    const onlyToken = authorization.replace('Bearer ', '');  
    const tokenDecodificado = await jwt.verify(onlyToken, app.auth.secret);
    
            // Envia al req el id del Usuario, par manipular en personas
    req.query.idUsuario = tokenDecodificado._id;
    req.usuario = tokenDecodificado;

    next();

  } catch (error) {
    res.status(401).json({
      finalizado: false,
      mensaje: error.message,
      datos: null
    });      
  }
}

module.exports = {
  verificarToken
}