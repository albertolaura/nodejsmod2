
 const {app, constants } = require('../config');
 const {mensajeExito, mensajeError} = require ('./Respuesta');
 const jwt = require('jsonwebtoken');

 const { UsuarioService } = require('../services');

 const login = async (req, res) => {
    try{
        const { usuario, contrasena } = req.body;

        //console.log('Authcontroller ->', req.body);
        //console.log('usuario ->', usuario);

        const respuesta = await UsuarioService.login(usuario, contrasena);

        if (!respuesta) {
            throw new Error('Error con sus credenciales');
        }

                // Elimina la columna contraseña, como ejemplo
        delete respuesta.contrasena;

        //Tiempo de expiracion
        const token = jwt.sign( 
            { ...respuesta, },
            app.auth.secret,
            { expiresIn: '4h'} 
            );

        return mensajeExito(res, 200, 
            'Sistema forma correcta',
             {...respuesta, token } 
            );

    } catch (error) {
        return mensajeError(res, 400, error.message, null);
    }
}

module.exports = {
    login
}