module.exports = {
  AuthController: require('./AuthController'),
  UsuarioController: require('./UsuarioController'),
  EstudianteController: require('./EstudianteController')
};
