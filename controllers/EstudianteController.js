
 const { app, constants } = require('../config');
 
 const { EstudianteModel } = require('../models');
 
 const axios = require('axios');

 const { EstudianteService } = require('../services');

 const {mensajeExito, mensajeError} = require ('./Respuesta');

        // http://localhost:3000/estudiantes 
        // http://localhost:3000/estudiantes?numeroDocumento=12345

 
 const listarEstudiantes = async ( req, res ) => {
    try{
        const { numeroDocumento } = req.query;
        const listaEstudiantes = await EstudianteService.findAll( { numeroDocumento: numeroDocumento } );
        return mensajeExito( res, 200, 'ok', listaEstudiantes);
    
    }   catch (error)  {
        return mensajeError( res, 400, error.message, null);
    }

 };
 
        // http://localhost:3000/estudiantes
        // {
        //    "nombres": "Jorge", 
        //    "apellidoPaterno": "Algarañaz",
        //    "numeroDocumento": "123456",
        //    "curso": "4to. B"
        // }

 const crearEstudiante = async (req, res) => {
     try{
         const datos = req.body;
         const estudianteCreado = await EstudianteService.guardar(datos);
         return mensajeExito(res, 201, 'Estudiante creado', estudianteCreado);

     } catch (error) {
         return mensajeError(res, 400, error.message, null);
     }
 }
 
        // PUT -> http://localhost:3000/estudiantes/612e9ffa08e5a42336776d56
        //{
        //    "nombres": "Jorge", 
        //    "apellidoPaterno": "Algarañaz",
        //    "numeroDocumento": "123456"
        //    "curso": "4to. B"
        //}

 const actualizarEstudiante = async (req, res) => {
    try{
        
        const  _id  = req.params.id;
        const datos = req.body;
        datos._id = _id;

        console.log("datos=", datos);
        console.log("_id", _id);
        
        const estudianteCreado = await EstudianteService.guardar(datos);
        return mensajeExito(res, 201, 'Estudiante modificado', estudianteCreado);

    } catch (error) {
        return mensajeError(res, 400, error.message, null);
    }
 };

        // http://localhost:3000/estudiantes/612415ab936713a1513cb0a1

const eliminarEstudiante = async (req, res) => {
    try{
        const _id = req.params.id;
        const estudianteEliminado = await EstudianteService.eliminar(_id);
        return mensajeExito(res, 201, 'Estudiante eliminado ', estudianteEliminado);

    } catch (error) {
        return mensajeError(res, 400, error.message, null);
    }
}


 module.exports = {
     listarEstudiantes,
     //generarToken, 
     crearEstudiante,
     actualizarEstudiante,
     eliminarEstudiante
 }
