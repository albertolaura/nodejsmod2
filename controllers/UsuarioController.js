
 const { app, constants } = require('../config');
 
 const { UsuarioModel } = require('../models');
 const { UsuarioService } = require('../services');
 
 const axios = require('axios');


 const {mensajeExito, mensajeError} = require ('./Respuesta');


 
// http://localhost:3000/usuario
// Authorization  = Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwZXJtaXNvcyI6WyJ1c3VhcmlvczpsaXN0YXIiLCJ1c3VhcmlvczpjcmVhciIsInVzdWFyaW9zOmVkaXRhciIsInVzdWFyaW9zOmVsaW1pbmFyIiwicGVyc29uYXM6bGlzdGFyIiwicGVyc29uYXM6Y3JlYXIiLCJwZXJzb25hczplZGl0YXIiLCJwZXJzb25hczplbGltaW5hciJdLCJfaWQiOiI2MTI1YjNlNzk4MWRlZjM4OTgxYmI5OGMiLCJ1c3VhcmlvIjoiYWRtaW4iLCJ1c3VhcmlvQ3JlYWNpb24iOiI2MTI1YjNlNzk4MWRlZjM4OTgxYmI5OGMiLCJpYXQiOjE2MzA0NTU4NjQsImV4cCI6MTYzMDQ3MDI2NH0.caKaYycTUaLRH396kArFtDgnxz-mDXmJEhfRkE2mqk8
    
 
 const listarUsuarios = async ( req, res ) => {
    try{
        const {numeroDocumento} = req.query;

        console.log('ListarUsuarios');

        const listarUsuarios = await UsuarioService.findAll( {numeroDocumento: numeroDocumento} );
        return mensajeExito( res, 200, 'ok', listarUsuarios);
    
    }   catch (error)  {
        return mensajeError( res, 400, error.message, null);
    }

 };
 
        // http://localhost:3000/usuarios
        // {     
        //    "usuario": "admin", 
        //    "contrasena": "admin",
        //    "permisos": [
        //         "usuarios:listar",
        //         "usuarios:crear",
        //         "usuarios:editar",
        //         "usuarios:eliminar",
        //         "estudiantes:listar",
        //         "estudiantes:crear",
        //         "estudiantes:editar",
        //         "estudiantes:eliminar"
        //     ],  
        // }


 const crearUsuario = async (req, res) => {
     try{
         const datos = req.body;
         const usuarioCreado = await UsuarioService.guardar(datos);
         return mensajeExito(res, 201, 'Usuario creado', usuarioCreado);

     } catch (error) {
         return mensajeError(res, 400, error.message, null);
     }
 }
 
        // PUT -> http://localhost:3000/usuarios/6127c0436d1970b8fa735d8e
        // {     
        //    "usuario": "itancara", 
        //    "contrasena": "itancara",
        //    "permisos": 
        //     [
        //         "estudiantes:listar",
        //     ]
        // }

 const actualizarUsuario = async (req, res) => {
    try{
        const { _id }  = req.params;    // req.params.id;
        const datos = req.body;
        datos._id = _id;

        const usuarioCreado = await UsuarioService.guardar(datos);
        return mensajeExito(res, 201, 'Usuario modificado', usuarioCreado);

    } catch (error) {
        return mensajeError(res, 400, error.message, null);
    }
 };

        // http://localhost:3000/usuarios/612415ab936713a1513cb0a1

const eliminarUsuario = async (req, res) => {
    try{
        
        const _id = req.params.id;

        const usuarioEliminado = await UsuarioService.eliminar(_id);
        return mensajeExito(res, 200, 'Usuario eliminado ', usuarioEliminado);

    } catch (error) {
        return mensajeError(res, 400, error.message, null);
    }
}


const login = async (req, res) => {
    try{
        
        const {usuario, contrasena} = req.body;
        const respuesta = await UsuarioService.login(usuario, contrasena);
        return mensajeExito(res, 200, 'Usuario identificado ', respuesta);

    } catch (error) {
        return mensajeError(res, 400, error.message, null);
    }
}
 
 module.exports = {
     listarUsuarios,
     crearUsuario,
     actualizarUsuario,
     eliminarUsuario,
     login
 }
