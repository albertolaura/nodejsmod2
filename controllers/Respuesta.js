const mensajeExito = (res, code = 200, mensaje = 'ok', datos = {} ) => {

    return res.status(code || 201).json(
        {
            finalizado: true,
            mensaje: mensaje,
            datos: datos
        }
    )
};

const mensajeError = ( res, code, mensaje, datos ) => {

    return res.status(code).json(
    {
        finalizado: false,
        mensaje: mensaje,
        datos: datos
    }
    )
};

module.exports = {
    mensajeExito, 
    mensajeError
}