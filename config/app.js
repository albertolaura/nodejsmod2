module.exports = {
    auth: {
        secret: 'LLAVE',
        exp: 60*4
    },
    expirationToken: 240,  //Minutos
    impuestos: {
        urlRecuperacionDatos: 'https://123',
        urlCreacionDatos: 'https://456'
    }
}
