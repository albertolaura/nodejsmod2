
const express = require('express');
const router = express.Router();

const { AuthController } = require('../controllers');
const { AuthMiddleware } = require('../middlewares');

router.post('/public/login', AuthController.login );


module.exports = router;
