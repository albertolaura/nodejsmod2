const express = require('express');
const router = express.Router();

const { UsuarioController } = require('../controllers');

const { AuthMiddleware, PermisosMiddleware } = require('../middlewares');

// usuarios:listar
//router.get('/usuarios', UsuarioController.listarUsuarios); 
router.get('/usuarios', AuthMiddleware.verificarToken, PermisosMiddleware.verificarPermisos ('usuarios:listar'), UsuarioController.listarUsuarios); 

// usuarios:crear
//router.post('/usuarios',  UsuarioController.crearUsuario ); 
router.post('/usuarios', AuthMiddleware.verificarToken, PermisosMiddleware.verificarPermisos ('usuarios:crear'), UsuarioController.crearUsuario ); 

// usuarios:editar
//router.put('/usuarios/:_id', UsuarioController.actualizarUsuario);
router.put('/usuarios/:_id', AuthMiddleware.verificarToken, PermisosMiddleware.verificarPermisos ('usuarios:editar'), UsuarioController.actualizarUsuario);

// usuarios:eliminar        
//router.delete('/usuarios/:id', UsuarioController.eliminarUsuario);
router.delete('/usuarios/:id', AuthMiddleware.verificarToken, PermisosMiddleware.verificarPermisos ('usuarios:eliminar'), UsuarioController.eliminarUsuario);

module.exports = router;
