const express = require('express');
const router = express.Router();
const { EstudianteController } = require('../controllers');
const { AuthMiddleware, PermisosMiddleware } = require('../middlewares');

// estudiantes:listar
//router.get('/estudiantes',  EstudianteController.listarEstudiantes); 
router.get('/estudiantes', AuthMiddleware.verificarToken, PermisosMiddleware.verificarPermisos ('estudiantes:listar'), EstudianteController.listarEstudiantes); 

// estudiantes:crear
//router.post('/estudiantes', EstudianteController.crearEstudiante );
router.post('/estudiantes', AuthMiddleware.verificarToken, PermisosMiddleware.verificarPermisos ('estudiantes:crear'), EstudianteController.crearEstudiante );

// estudiantes:editar
//router.put('/estudiantes/:_id', EstudianteController.actualizarEstudiante);
router.put('/estudiantes/:_id', AuthMiddleware.verificarToken, PermisosMiddleware.verificarPermisos ('estudiantes:editar'), EstudianteController.actualizarEstudiante);

// estudiantes:eliminar        
//router.delete('/estudiantes/:id', EstudianteController.eliminarEstudiante);
router.delete('/estudiantes/:id', AuthMiddleware.verificarToken, PermisosMiddleware.verificarPermisos ('estudiantes:eliminar'), EstudianteController.eliminarEstudiante);

module.exports = router;
