const { PersonaModel } = require('../models');

//  const findAll =  async (modelo) => {
//     const count = await modelo.count();
//     const rows = await modelo.find(params);
//     return {  count: count,   rows: rows   };
// };


const findAndCountAll =  async (modelo, query) => {

    console.log('Query repositorio=', query);
    const count = await modelo.count(query); // query
    const rows = await modelo.find(query);   // query
    return { count, rows }; // 
};


    // buscar el registro que contiene una id
    // si no tiene id, procede a crear el registro

const createOrUpdate = async (modelo, datos) => {

    console.log("desde Repositorio");

    if (datos._id) {

        const _existeRegistro = await modelo.findbyId(datos._id);
        if (_existeRegistro) {
            const _registroActualizado = await modelo.update ({ _id: datos._id },{ $set: datos });
            return _registroActualizado;
        } else {
            const _registroCreado = await modelo.create(datos);
            return _registroCreado;
        }
       
    }
    const _registroCreado = await modelo.create(datos);
    return _registroCreado;
}

const deleteItem = (modelo, _id) => {
    return modelo.findbyIdAndRemove(_id);
}

const findOne = (modelo, params) => {
    return modelo.findOne(params);
}

module.exports = {
    findAndCountAll,
    createOrUpdate,
    deleteItem,
    findOne
        //findAll, 


}
