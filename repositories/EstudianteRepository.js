
const { EstudianteModel} = require('../models');
//const { Repository } = require ('./Repository');

const findAndCountAll = async ( query ) => {
     numeroDocumento = query.numeroDocumento;   // ejemplo 1 dia antes
      if (numeroDocumento ) {
         const personas = await EstudianteModel.find(query);  
         return estudiantes;
      } else {
        const estudiantes = await EstudianteModel.find();  
        return estudiantes;
      }
 }

 const createOrUpdate = async( datos ) => {

    if (datos._id) {

        const _existeRegistro = await EstudianteModel.findbyId(datos._id);
        if (_existeRegistro) {
            const _registroActualizado = await EstudianteModel.update ({ _id: datos._id },{ $set: datos });
            return _registroActualizado;
        } else {
            const _registroCreado = await EstudianteModel.create(datos);
            return _registroCreado;
        }
       
    }
    const _registroCreado = await EstudianteModel.create(datos);
    return _registroCreado;

 }


 const deleteItem = async (_id) => {
    return EstudianteModel.findByIdAndRemove(_id);
 }

module.exports = {
    findAndCountAll, 
    createOrUpdate, 
    deleteItem
}
