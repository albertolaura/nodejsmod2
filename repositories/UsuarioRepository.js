
//const {PersonaRepository} = require('.');
const { UsuarioModel} = require('../models');
const { Repository } = require ('../repositories');

const findAndCountAll = async ( query ) => {

     numeroDocumento = query.numeroDocumento;

     if (numeroDocumento ) {
        const usuario = await UsuarioModel.find(query);  
        return usuario;
     } else {
        const usuario = await UsuarioModel.find();  
        return usuario;
     }
     
     
 }

 const createOrUpdate = async( datos ) => {

    console.log("usuarioRepository,createUpdate= ", datos);
    console.log("_id=", datos._id);
    
    if (datos._id) {
        const _existeRegistro = await UsuarioModel.findById(datos._id);
        if (_existeRegistro) {
            const _registroActualizado = await UsuarioModel.update ({ _id: datos._id },{ $set: datos });
            return _registroActualizado;
        } else {
            const _registroCreado = await UsuarioModel.create(datos);
            return _registroCreado;
        }
       
    }
    const _registroCreado = await UsuarioModel.create(datos);
    return _registroCreado;

 }


 const deleteItem = async (_id) => {
    return UsuarioModel.findByIdAndRemove(_id);
 }

 const findOne = async (params) => {
     const respuesta = await UsuarioModel.findOne(params);
    
     if (!respuesta) {
         return null;
     }

     return respuesta._doc;     // Nos devuelve solo el doc
 }



module.exports = {
    //findAll: Repository.findAll(UsuarioModel), 
    //findAndCountAll: (query) => Repository.findAndCountAll (UsuarioModel, query) ,
    findAndCountAll, 
    //createOrUpdate: (datos) => Repository.createOrUpdate (UsuarioModel, datos) , 
    createOrUpdate, 
    //deleteItem: (_id) => Repository.deleteItem(UsuarioModel, _id)
    deleteItem,
    //findOne: params => Repository.findOne(UsuarioModel, params) 
    findOne
};
